Dear CRAN Team,
this is a resubmission of package 'fakemake'. I have added the following changes:

* Added cyclocomp to the "package" makelist.

Please upload to CRAN.
Best, Andreas Dominik

# Package fakemake 1.3.0
## Test  environments 
- R Under development (unstable) (2018-07-01 r74950)
  Platform: x86_64-pc-linux-gnu (64-bit)
  Running under: Devuan GNU/Linux 2 (ascii)
- R version 3.3.3 (2017-03-06)
  Platform: x86_64-pc-linux-gnu (64-bit)
  Running under: Devuan GNU/Linux 2 (ascii)
- R version 3.5.0 (2017-01-27)
  Platform: x86_64-pc-linux-gnu (64-bit)
  Running under: Ubuntu 14.04.5 LTS
- win-builder (devel)

## R CMD check results
0 errors | 0 warning  | 0 notes
