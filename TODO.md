- Don't forget to tag commit  e0587d866a417885fb83b522ce79bf2d49b62faf  as  1.2.0 , once package is on CRAN.
- Use provide_make_list("standard") in vignette
- write a test for tryCatch!! The example runs, the test (tried testthat as
  well) crashes...!
- make uses eval(parse(text = )). Isn't there a better way?
- enhance vignette
  * testthat output is not caught to sink? Hadley, ...
